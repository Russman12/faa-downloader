package com.ruinedkingdom.csvformatter;

class DBConnection {
    static void importCSV(FileExport fileExport) {

        Runtime r = Runtime.getRuntime();
        Process p;
        String command = "data/import.sh " + fileExport.getName() + " " + fileExport.getAbsolutePath();
        try {
            Logger.instance().log("Executing: " + command);
            p = r.exec(command);
            p.waitFor();
            Logger.instance().log("Command Complete: " + command);
        } catch (Exception e){
            Logger.instance().log(e.getMessage());
        }
    }
}
