package com.ruinedkingdom.csvformatter.model;

@SuppressWarnings("unused")
public class CertificateRating {

    private static final CertificateRating INSTANCE = new CertificateRating();

    private String certificateId;
    private String ratingCertificateLevel;
    private String rating;

    public static CertificateRating getInstance() {
        return INSTANCE;
    }

    public String getCertificateId() {
        return certificateId;
    }

    public String getRatingCertificateLevel() {
        return ratingCertificateLevel;
    }

    public String getRating() {
        return rating;
    }

    public void setCertificateId(String certificateId) {
        this.certificateId = certificateId;
    }

    public void setRatingCertificateLevel(String ratingCertificateLevel) {
        this.ratingCertificateLevel = ratingCertificateLevel;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

}
