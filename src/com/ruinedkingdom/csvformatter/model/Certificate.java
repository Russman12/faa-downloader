package com.ruinedkingdom.csvformatter.model;

@SuppressWarnings("ALL")
public class Certificate {

    private static final Certificate INSTANCE = new Certificate();

    private String id;
    private String pilotId;
    private String recordType;
    private String certificateType;
    private String certificateLevel;
    private String certificateExpireDate;

    public static Certificate getInstance() {
        return INSTANCE;
    }

    public String getId() {
        return this.pilotId + this.recordType;
    }

    public String getPilotId() {
        return pilotId;
    }

    public String getRecordType() {
        return recordType;
    }

    public String getCertificateType() {
        return certificateType;
    }

    public String getCertificateLevel() {
        return certificateLevel;
    }

    public String getCertificateExpireDate() {
        return certificateExpireDate;
    }

    public void setPilotId(String pilotId) {
        this.pilotId = pilotId;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public void setCertificateType(String certificateType) {
        this.certificateType = certificateType;
    }

    public void setCertificateLevel(String certificateLevel) {
        this.certificateLevel = certificateLevel;
    }

    public void setCertificateExpireDate(String certificateExpireDate) {
        this.certificateExpireDate = certificateExpireDate;
    }

}
