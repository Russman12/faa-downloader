package com.ruinedkingdom.csvformatter.model;

@SuppressWarnings("unused")
public class Individual {

    private static final Individual INSTANCE = new Individual();

    private String uniqueId;
    private String recordType;
    private String firstMiddleName;
    private String lastNameSuffix;
    private String street1;
    private String street2;
    private String city;
    private String state;
    private String zipCode;
    private String countryName;
    private String region;
    private String medicalClass;
    private String medicalDate;
    private String medicalExpireDate;

    public static Individual getInstance() {
        return INSTANCE;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public String getRecordType() {
        return recordType;
    }

    public String getFirstMiddleName() {
        return firstMiddleName;
    }

    public String getLastNameSuffix() {
        return lastNameSuffix;
    }

    public String getStreet1() {
        return street1;
    }

    public String getStreet2() {
        return street2;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getRegion() {
        return region;
    }

    public String getMedicalClass() {
        return medicalClass;
    }

    public String getMedicalDate() {
        return medicalDate;
    }

    public String getMedicalExpireDate() {
        return medicalExpireDate;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public void setFirstMiddleName(String firstMiddleName) {
        this.firstMiddleName = firstMiddleName;
    }

    public void setLastNameSuffix(String lastNameSuffix) {
        this.lastNameSuffix = lastNameSuffix;
    }

    public void setStreet1(String street1) {
        this.street1 = street1;
    }

    public void setStreet2(String street2) {
        this.street2 = street2;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setMedicalClass(String medicalClass) {
        this.medicalClass = medicalClass;
    }

    public void setMedicalDate(String medicalDate) {
        this.medicalDate = medicalDate;
    }

    public void setMedicalExpireDate(String medicalExpireDate) {
        this.medicalExpireDate = medicalExpireDate;
    }

}
