package com.ruinedkingdom.csvformatter.model;

@SuppressWarnings("unused")
public class CertificateTypeRating {

    private static final CertificateTypeRating INSTANCE = new CertificateTypeRating();

    private String certificateId;
    private String ratingCertificateLevel;
    private String typeRating;

    public static CertificateTypeRating getInstance() {
        return INSTANCE;
    }

    public String getCertificateId() {
        return certificateId;
    }

    public String getRatingCertificateLevel() {
        return ratingCertificateLevel;
    }

    public String getTypeRating() {
        return typeRating;
    }

    public void setCertificateId(String certificateId) {
        this.certificateId = certificateId;
    }

    public void setRatingCertificateLevel(String ratingCertificateLevel) {
        this.ratingCertificateLevel = ratingCertificateLevel;
    }

    public void setTypeRating(String typeRating) {
        this.typeRating = typeRating;
    }

}
