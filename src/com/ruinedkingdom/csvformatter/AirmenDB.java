package com.ruinedkingdom.csvformatter;

import net.lingala.zip4j.exception.ZipException;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Objects;

import net.lingala.zip4j.core.ZipFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


class AirmenDB {
    private static final String ROOT_DIRECTORY = "data";
    private static final String AIRMEN_DB_ZIP_NAME = ROOT_DIRECTORY + "/airmenDB.zip";
    private static final String AIRMEN_DB_UNZIP_NAME = ROOT_DIRECTORY + "/imports";
    private static Boolean isChanged;

    public static void getAirmenDB() throws Exception {
        Logger.instance().log("Retrieving Airmen Database...");

        URL endpointURL = getEndpointURL();
        if(endpointURL != null) {
            Logger.instance().log("Valid Endpoint Found.");
            downloadDBZip(endpointURL);
        } else {
            throw new Exception("Valid Endpoint URL not found.");
        }
        Logger.instance().log("Airmen Database Retrieved.");

        if(getIsChanged()) {
            unzipDB();
        }
    }

    @Nullable
    private static URL getEndpointURL() {
        Logger.instance().log("Determining Endpoint...");

        //Constants
        final int MAX_ATTEMPTS = 5;

        //Date Variables
        DateFormat dateFormat = new SimpleDateFormat("MMyyyy");
        Calendar cal = Calendar.getInstance();

        //URL Variables
        String baseURL = "https://registry.faa.gov/database/";
        URL url = null;

        try {
            //iterator variable
            int i = 0;

            do {
                url = new URL(baseURL + "FX" + dateFormat.format(cal.getTime()) + ".zip");

                cal.add(Calendar.MONTH, -1);
                i++;
            } while(!isValidEndpoint(url) && i != MAX_ATTEMPTS);

            if(i == MAX_ATTEMPTS) {
                return null;
            }

        } catch (MalformedURLException e) {
            Logger.instance().log(e.getMessage());
            e.printStackTrace();
        }

        return url;
    }

    private static boolean isValidEndpoint(@NotNull URL url) {
        Logger.instance().log("Validating endpoint: " + url.toString() + "...");
        try {
            HttpURLConnection huc = (HttpURLConnection) url.openConnection();
            huc.setRequestMethod("GET");
            huc.connect();

            return huc.getResponseCode() == 200;

        } catch (Exception e) {
            Logger.instance().log("Error validating endpoint.");
            Logger.instance().log(e.getMessage());
            return false;
        }
    }

    private static void downloadDBZip(URL endpointURL) {
        Logger.instance().log("Downloading Airmen Database...");

        try {
            FileOutputStream fos = new FileOutputStream(AIRMEN_DB_ZIP_NAME);

            fos.getChannel().transferFrom(Channels.newChannel(endpointURL.openStream()), 0, Long.MAX_VALUE);
            fos.close();

            Logger.instance().log("Airmen Database Downloaded.");
        } catch (Exception e) {
            Logger.instance().log("Error Downloading Airmen Database.");
            e.printStackTrace();
        }
    }

    public static Boolean getIsChanged() {
        if(isChanged == null) {
            Logger.instance().log("Determining changes");
            String newChecksum = "", oldChecksum = "";
            final String CHECKSUM_ABSOLUTE_PATH = ROOT_DIRECTORY + "/checksum";


            try {
                newChecksum = Hash.MD5.checksum(new File(AIRMEN_DB_ZIP_NAME));

                BufferedReader bufferedReader = new BufferedReader(new FileReader(CHECKSUM_ABSOLUTE_PATH));
                oldChecksum = bufferedReader.readLine();
            } catch (IOException e) {
                Logger.instance().log(e.getMessage());
            }

            if (!Objects.equals(newChecksum, oldChecksum)) {
                try (PrintWriter printWriter = new PrintWriter(CHECKSUM_ABSOLUTE_PATH)) {
                    printWriter.println(newChecksum);
                } catch (IOException e) {
                    Logger.instance().log(e.getMessage());
                }

                Logger.instance().log("Data changed");
                isChanged = true;
            } else {
                Logger.instance().log("Data not changed");
                isChanged = false;
            }
        }

        return isChanged;
    }

    private static void unzipDB() {
        Logger.instance().log("Unzipping Database File...");
        try {
            ZipFile zipFile = new ZipFile(AIRMEN_DB_ZIP_NAME);
            zipFile.extractAll(AIRMEN_DB_UNZIP_NAME);
        } catch (ZipException e) {
            Logger.instance().log(e.getMessage());
        }
    }
}
