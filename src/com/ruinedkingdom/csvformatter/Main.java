package com.ruinedkingdom.csvformatter;

import java.util.HashSet;
import java.util.Set;

class Main {

    public static void main(String[] args) {
        Logger.instance().log("Program Started.");

        if(!(args.length > 0 && args[0].contains("--skipdl"))) {
            //retrieve latest DB from web
            try {
                AirmenDB.getAirmenDB();
            } catch (Exception e) {
                Logger.instance().log(e.getMessage());
                e.printStackTrace();

                closeApp(100);
            }
        }

        if(AirmenDB.getIsChanged()) {
            FixedLengthFileReader flfr = new FixedLengthFileReader();

            Set<FileExport> fileExportSet = new HashSet<>(flfr.getFileExports());
            fileExportSet.parallelStream().forEach(DBConnection::importCSV);
        }

        //perform closing operations
        closeApp(0);
    }

    private static void closeApp(int statusCode) {
        Logger.instance().log("Initiating Shutdown Procedure.");
        Logger.instance().closeStream();

        System.out.println("Program Complete.\n\n");

        System.exit(statusCode);
    }
}
