package com.ruinedkingdom.csvformatter;

class FileExport {
    private final String name;
    private final String absolutePath;

    public FileExport(String name) {
        this.name = name;
        this.absolutePath = System.getProperty("user.dir") + "/data/exports/" + name + ".csv";
    }

    public String getName() {
        return name;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }
}
