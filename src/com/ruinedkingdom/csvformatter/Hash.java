package com.ruinedkingdom.csvformatter;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.MessageDigest;

public enum Hash {
    MD5("MD5");

    private final String algorithm;

    Hash(@SuppressWarnings("SameParameterValue") String algorithm) {
        this.algorithm = algorithm;
    }

    public String checksum(File input) {
        try (InputStream in = new FileInputStream(input)) {
            MessageDigest digest = MessageDigest.getInstance(this.algorithm);
            byte[] block = new byte[4096];
            int length;
            while ((length = in.read(block)) > 0) {
                digest.update(block, 0, length);
            }

            return byteToHex(digest.digest());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String byteToHex(byte[] bs) {
        StringBuilder hex = new StringBuilder();
        for (byte b : bs) {
            int i = b & 0xFF;
            hex.append(Integer.toHexString(i));
        }
        return hex.toString();
    }

}
