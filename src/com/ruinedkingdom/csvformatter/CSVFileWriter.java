package com.ruinedkingdom.csvformatter;

import java.io.FileWriter;

import org.supercsv.io.*;
import org.supercsv.prefs.CsvPreference;

class CSVFileWriter {

    private ICsvBeanWriter beanWriter;
    private final String[] header;
    private final FileExport fileExport;

    public CSVFileWriter(String fileName, String[] header) {

        this.fileExport = new FileExport(fileName);

        this.header = header;

        try {
            this.beanWriter = new CsvBeanWriter(new FileWriter(fileExport.getAbsolutePath()), CsvPreference.STANDARD_PREFERENCE);

            beanWriter.writeHeader(this.header);
        } catch (Exception e) {
            Logger.instance().log("error: " + e.getMessage());
        }
    }

    /**
     * This method performs the write operation to the file
     */
    public void writeLine(Object obj) {
        try {
            this.beanWriter.write(obj, this.header);
        } catch(Exception e) {
            Logger.instance().log("error: " + e.getMessage());
        }
    }

    public void closeCSVFileWriter() {
        try {
            this.beanWriter.close();
        } catch (Exception e) {
            Logger.instance().log("error: " + e.getMessage());
        }
    }

    public FileExport getFileExport() {
        return fileExport;
    }
}
