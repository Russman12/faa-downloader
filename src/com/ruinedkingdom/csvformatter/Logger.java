package com.ruinedkingdom.csvformatter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

class Logger {
    private static PrintWriter printWriter;
    private static final Logger LOGGER = new Logger();

    private Logger() {
        try {
            printWriter = new PrintWriter(new FileOutputStream("log"));
        } catch (FileNotFoundException e) {
            System.out.println("Error writing file.");
            e.printStackTrace();
        }
    }

    public static Logger instance() {
        return LOGGER;
    }

    public void log(String message) {
        System.out.println(message);
        printWriter.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()) + ": " + message);
        printWriter.flush();
    }

    public void closeStream() {
        printWriter.close();
    }
}
