package com.ruinedkingdom.csvformatter;

import com.ruinedkingdom.csvformatter.model.*;

import java.io.*;
import java.util.Arrays;
import java.util.List;

class FixedLengthFileReader {
    private final CSVFileWriter individualCSVFileWriter;
    private final CSVFileWriter certificateCSVFileWriter;
    private final CSVFileWriter certificateRatingCSVFileWriter;
    private final CSVFileWriter certificateTypeRatingCSVFileWriter;

    public FixedLengthFileReader() {
        //set CSV File Writers
        this.individualCSVFileWriter = new CSVFileWriter(
                "person",
                new String[]{
                        "uniqueId",
                        "recordType",
                        "firstMiddleName",
                        "lastNameSuffix",
                        "street1",
                        "street2",
                        "city",
                        "state",
                        "zipCode",
                        "countryName",
                        "region",
                        "medicalClass",
                        "medicalDate",
                        "medicalExpireDate"
                }
        );

        this.certificateCSVFileWriter = new CSVFileWriter(
                "certification",
                new String[]{"id", "pilotId", "recordType", "certificateType", "certificateLevel", "certificateExpireDate"}
        );

        this.certificateRatingCSVFileWriter  = new CSVFileWriter(
                "certification_rating",
                new String[]{"certificateId", "ratingCertificateLevel", "rating"}
        );

        this.certificateTypeRatingCSVFileWriter  = new CSVFileWriter(
                "certification_type_rating",
                new String[]{"certificateId", "ratingCertificateLevel", "typeRating"}
        );


        readPilotFile();
        readNonPilotFile();

        this.individualCSVFileWriter.closeCSVFileWriter();
        this.certificateCSVFileWriter.closeCSVFileWriter();
        this.certificateRatingCSVFileWriter.closeCSVFileWriter();
        this.certificateTypeRatingCSVFileWriter.closeCSVFileWriter();
    }

    private void readPilotFile() {
        Logger.instance().log("Reading Pilot File...");

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("data/imports/PILOT.txt"));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                boolean isPilotRecord = line.substring(8, 10).equals("00");
                String pilotId = line.substring(0, 8).trim();

                if (isPilotRecord) {
                    Individual individual = Individual.getInstance();
                    individual.setUniqueId(pilotId);
                    individual.setRecordType(line.substring(8, 10).trim());
                    individual.setFirstMiddleName(line.substring(10, 40).trim());
                    individual.setLastNameSuffix(line.substring(40, 70).trim());
                    individual.setStreet1(line.substring(70, 103).trim());
                    individual.setStreet2(line.substring(103, 136).trim());
                    individual.setCity(line.substring(136, 153).trim());
                    individual.setState(line.substring(153, 155).trim());
                    individual.setZipCode(line.substring(155, 165).trim());
                    individual.setCountryName(line.substring(165, 183).trim());
                    individual.setRegion(line.substring(183, 185).trim());
                    individual.setMedicalClass(line.substring(185, 186).trim());
                    individual.setMedicalDate(line.substring(186, 192).trim());
                    individual.setMedicalExpireDate(line.substring(192, 198).trim());

                    //write to file
                    individualCSVFileWriter.writeLine(individual);
                } else {
                    Certificate cert = Certificate.getInstance();
                    cert.setPilotId(pilotId);
                    cert.setRecordType(line.substring(8, 10).trim());
                    cert.setCertificateType(line.substring(10, 11).trim());
                    cert.setCertificateLevel(line.substring(11, 12).trim());
                    cert.setCertificateExpireDate(line.substring(12, 20).trim());

                    //write to file
                    certificateCSVFileWriter.writeLine(cert);

                    String ratingsStr = line.substring(20, 130);
                    final int ratingLength = 10;
                    for(int i = 0; i < 11; i++) {
                        String ratingStr = ratingsStr.substring(i*ratingLength, (i+1)*ratingLength);
                        if(!ratingStr.trim().isEmpty()) {
                            CertificateRating cr = CertificateRating.getInstance();
                            cr.setCertificateId(cert.getId());
                            cr.setRatingCertificateLevel(ratingStr.substring(0,1).trim());
                            cr.setRating(ratingStr.substring(2,10).trim());

                            //write to file
                            certificateRatingCSVFileWriter.writeLine(cr);
                        }
                    }

                    String typeRatingsStr = line.substring(130, 1120);
                    for(int i = 0; i < 99; i++) {
                        String ratingTypeStr = typeRatingsStr.substring(i*ratingLength, (i+1)*ratingLength);
                        if(!ratingTypeStr.trim().isEmpty()) {
                            CertificateTypeRating ctr = CertificateTypeRating.getInstance();
                            ctr.setCertificateId(cert.getId());
                            ctr.setRatingCertificateLevel(ratingTypeStr.substring(0,1).trim());
                            ctr.setTypeRating(ratingTypeStr.substring(2,10).trim());

                            //write to file
                            certificateTypeRatingCSVFileWriter.writeLine(ctr);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Logger.instance().log(e.getMessage());

        }
    }

    private void readNonPilotFile() {
        Logger.instance().log("Reading NonPilot File...");

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("data/imports/NONPILOT.txt"));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                boolean isPilotRecord = line.substring(8, 10).equals("00");
                String pilotId = line.substring(0, 8).trim();

                if (isPilotRecord) {
                    Individual individual = Individual.getInstance();
                    individual.setUniqueId(pilotId);
                    individual.setRecordType(line.substring(8, 10).trim());
                    individual.setFirstMiddleName(line.substring(10, 40).trim());
                    individual.setLastNameSuffix(line.substring(40, 70).trim());
                    individual.setStreet2(line.substring(103, 136).trim());
                    individual.setStreet1(line.substring(70, 103).trim());
                    individual.setCity(line.substring(136, 153).trim());
                    individual.setState(line.substring(153, 155).trim());
                    individual.setZipCode(line.substring(155, 165).trim());
                    individual.setCountryName(line.substring(165, 183).trim());
                    individual.setRegion(line.substring(183, 185).trim());
                    individual.setMedicalClass(line.substring(185, 186).trim());
                    individual.setMedicalDate(line.substring(186, 192).trim());
                    individual.setMedicalExpireDate(line.substring(192, 198).trim());

                    //write to file
                    individualCSVFileWriter.writeLine(individual);
                } else {

                    Certificate cert = Certificate.getInstance();
                    cert.setPilotId(pilotId);
                    cert.setRecordType(line.substring(8, 10).trim());
                    cert.setCertificateType(line.substring(10, 11).trim());
                    cert.setCertificateLevel(line.substring(11, 12).trim());
                    cert.setCertificateExpireDate(line.substring(12, 20).trim());

                    //write to file
                    certificateCSVFileWriter.writeLine(cert);

                    String ratingsStr = line.substring(20, 130);
                    final int ratingLength = 10;
                    for(int i = 0; i < 11; i++) {
                        String ratingStr = ratingsStr.substring(i*ratingLength, (i+1)*ratingLength);
                        if(!ratingStr.trim().isEmpty()) {
                            CertificateRating cr = CertificateRating.getInstance();
                            cr.setCertificateId(cert.getId());
                            cr.setRatingCertificateLevel(ratingStr.substring(0,1).trim());
                            cr.setRating(ratingStr.substring(2,10).trim());

                            //write to file
                            certificateRatingCSVFileWriter.writeLine(cr);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Logger.instance().log(e.getMessage());

        }
    }

    public List<FileExport> getFileExports() {

        return Arrays.asList(
                this.individualCSVFileWriter.getFileExport(),
                this.certificateCSVFileWriter.getFileExport(),
                this.certificateRatingCSVFileWriter.getFileExport(),
                this.certificateTypeRatingCSVFileWriter.getFileExport()
        );

    }
}
