#!/bin/bash

#get arguments
tableName=$1
absolutePath=$2

#import to temp collection
mongoimport --db faa --collection ${tableName}\_tmp --type csv --file ${absolutePath} --headerline --drop

#set indices
if [ ${tableName} = "person" ]
then
    indexCommand="db."${tableName}"_tmp.createIndex({uniqueId: 1}, {unique: true});"
    mongo faa --eval "$indexCommand"
fi

#rename temp table
renameCommand="db."${tableName}"_tmp.renameCollection(\"$tableName\", true);"
mongo faa --eval "$renameCommand";


